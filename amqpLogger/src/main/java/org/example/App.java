package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 */

public class App {
    private static final Logger logger = LoggerFactory.getLogger(
            App.class);

    public static void thread(Runnable runnable, boolean daemon) {
        Thread brokerThread = new Thread(runnable);
        brokerThread.setDaemon(daemon);
        brokerThread.start();
    }

    public static void main(String[] args) {
        logger.trace("AAAAAAAAAAAAAA");
        logger.info("Just a log message.");
        logger.info("Just a log message.");
        logger.info("Just a log message.");
        logger.info("Just a log message.");
        logger.error("AAAAAAAAAAAAAA");
        thread(new ServerInfoSubscriber(), false);

    }
}
