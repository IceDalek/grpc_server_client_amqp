create table IF NOT EXISTS students(
                                       id int PRIMARY key,
                                       name varchar (40),
                                       lastName varchar (40),
                                       subject varchar(40),
                                       course int

);
create table IF NOT EXISTS teachers(
                                       id int PRIMARY key,
                                       name varchar (40),
                                       lastName varchar (40),
                                       subject varchar (40)
);
create table IF NOT EXISTS students_teachers(
                                                student_id int NOT NULL,
                                                teacher_id int NOT NULL,
                                                PRIMARY KEY (student_id, teacher_id),
                                                FOREIGN KEY (teacher_id) REFERENCES teachers(id) ON UPDATE CASCADE,
                                                FOREIGN KEY (student_id) REFERENCES students(id) ON UPDATE CASCADE
);
insert into students(id, name, lastName, subject, course) VALUES
(1, '')
update students set id = 1, name = 2, last_name = 3, subject = 4, course = 5 where id = 1