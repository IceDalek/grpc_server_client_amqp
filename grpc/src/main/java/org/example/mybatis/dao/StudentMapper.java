package org.example.mybatis.dao;

import org.apache.ibatis.annotations.*;
import org.example.mybatis.entity.StudentEntity;

import java.util.List;

public interface StudentMapper {
    /*
        private int id;
    private String name;
    private String lastName;
    private String subject;
    private int course;
     */
    @Select("select * from students where id = #{id}")
    StudentEntity getStudentById(Integer id);

    @Select("Select * from students")
    List<StudentEntity> getAllStudents();

    @Insert("insert into students(id, name, lastName, subject, course) VALUES \n" +
            "(#{id}, #{name}, #{lastName}, #{subject}, #{course})")
    void createNewStudent(StudentEntity studentEntity);

    @Delete("delete from students where id = #{id}")
    void deleteStudentById(Integer id);

    @Update("update students set id = #{studentEntity.id}, name = #{studentEntity.name}," +
            " last_name = #{studentEntity.lastName}, subject = #{studentEntity.subject}," +
            " course = #{studentEntity.course} where id = #{id}")
    void updateStudentById(@Param("StudentEntity") StudentEntity studentEntity, @Param("id") Integer id);

}
