package org.example.mybatis.service;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.mybatis.dao.StudentMapper;
import org.example.mybatis.dao.TeacherMapper;
import org.example.mybatis.entity.StudentEntity;
import org.example.mybatis.entity.TeacherEntity;

import java.util.List;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;

public class StudentService {
    private StudentMapper studentMapper;

    private SqlSessionFactory createSqlSession() {
        SqlSessionFactory sqlSessionFactory = null;
        Reader reader;
        String resource = "src/main/resources/mybatis-config.xml";// path of the mybatis configuration file.
        File file = new File(resource);
        System.out.println(file.exists());
        try {
            reader = new FileReader(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            sqlSessionFactory.getConfiguration().setMapUnderscoreToCamelCase(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlSessionFactory;
    }

    public StudentEntity getStudentById(int id) {
        StudentEntity studentEntity;
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        studentMapper = sqlSessionFactory.openSession().getMapper(StudentMapper.class); //Создаем маппер, из которого и будем вызывать методы getSubscriberById и getSubscribers
        studentEntity = studentMapper.getStudentById(id);
        return studentEntity;
    }

    public List<StudentEntity> getAllStudents() {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        studentMapper = sqlSessionFactory.openSession().getMapper(StudentMapper.class);
        return studentMapper.getAllStudents();
    }

    public StudentEntity createNewStudent(StudentEntity studentEntity) {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        studentMapper = sqlSessionFactory.openSession().getMapper(StudentMapper.class);
        studentMapper.createNewStudent(studentEntity);
        return studentMapper.getStudentById(studentEntity.getId());
    }

    public StudentEntity updateStudentById(StudentEntity studentEntity, Integer id) {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        studentMapper = sqlSessionFactory.openSession().getMapper(StudentMapper.class);
        studentMapper.updateStudentById(studentEntity, id);
        return studentEntity;
    }

    public StudentEntity deleteStudentById(Integer id) {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        studentMapper = sqlSessionFactory.openSession().getMapper(StudentMapper.class);
        StudentEntity ret = studentMapper.getStudentById(id);
        studentMapper.deleteStudentById(id);
        return ret;
    }
}
