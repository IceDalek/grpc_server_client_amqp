package org.example.mybatis.service;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.mybatis.dao.TeacherMapper;
import org.example.mybatis.entity.TeacherEntity;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

public class TeacherService {
    private TeacherMapper teacherMapper;

    private SqlSessionFactory createSqlSession() {
        SqlSessionFactory sqlSessionFactory = null;
        Reader reader;
        String resource = "src/main/resources/mybatis-config.xml";// path of the mybatis configuration file.
        File file = new File(resource);
        System.out.println(file.exists());
        try {
            reader = new FileReader(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            sqlSessionFactory.getConfiguration().setMapUnderscoreToCamelCase(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlSessionFactory;
    }

    public TeacherEntity getTeacherById(int id) {
        TeacherEntity teacherEntity;
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        teacherMapper = sqlSessionFactory.openSession().getMapper(TeacherMapper.class); //Создаем маппер, из которого и будем вызывать методы getSubscriberById и getSubscribers
        teacherEntity = teacherMapper.getTeacherById(id);
        System.out.println(teacherEntity);
        return teacherEntity;
    }

    public List<TeacherEntity> getAllTeachers() {
        List<TeacherEntity> teachersList;
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        teacherMapper = sqlSessionFactory.openSession().getMapper(TeacherMapper.class);
        teachersList = teacherMapper.getAllTeachers();
        return teachersList;
    }

    public TeacherEntity deleteTeacherById(Integer id) {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        teacherMapper = sqlSessionFactory.openSession().getMapper(TeacherMapper.class);
        TeacherEntity teacherEntity = teacherMapper.getTeacherById(id);
        teacherMapper.deleteTeacherById(id);
        return teacherEntity;
    }

    public TeacherEntity updateTeacherById(Integer id, TeacherEntity teacherEntity) {
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        teacherMapper = sqlSessionFactory.openSession().getMapper(TeacherMapper.class);
        teacherMapper.updateTeacherById(teacherEntity, id);
        TeacherEntity ret = teacherMapper.getTeacherById(id);
        return ret;
    }
    @Transactional
    public TeacherEntity addNewTeacher(TeacherEntity teacherEntity){
        SqlSessionFactory sqlSessionFactory = createSqlSession();
        teacherMapper = sqlSessionFactory.openSession().getMapper(TeacherMapper.class);
        teacherMapper.addNewTeacher(teacherEntity);
        System.out.println("add new teacher???");
        TeacherEntity ret = teacherMapper.getTeacherById(teacherEntity.getId());
        return ret;
    }


}
