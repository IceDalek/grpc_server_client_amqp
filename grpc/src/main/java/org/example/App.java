package org.example;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import lombok.extern.slf4j.Slf4j;
import org.example.service.TeacherServiceImpl;

import java.io.IOException;


/**
 * Hello world!
 *
 */
@Slf4j
public class App 
{

    public static void main( String[] args ) throws IOException, InterruptedException {

        Server server = ServerBuilder.forPort(8020).
                addService(new TeacherServiceImpl())
                .build();
        server.start();
        System.out.println("started server");
        server.awaitTermination();
    }
}
