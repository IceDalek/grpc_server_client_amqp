package org.example.service;

import com.example.grpc.StudentsServiceGrpc;
import com.example.grpc.TestService;
import io.grpc.stub.StreamObserver;
import org.example.amqp.ServerInfoPublisher;
import org.example.mybatis.entity.StudentEntity;

import org.example.mybatis.entity.TeacherEntity;
import org.example.mybatis.service.StudentService;

import java.time.LocalDateTime;
import java.util.List;

public class StudentServiceImpl extends StudentsServiceGrpc.StudentsServiceImplBase {
    private StudentService studentService = new StudentService();
    @Override
    public void getStudentById(TestService.GetStudentByIdRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        System.out.println(request);

        StudentEntity studentEntity = studentService.getStudentById(request.getId());
        TestService.GetStudentsByIdResponse response = TestService.GetStudentsByIdResponse.newBuilder()
                .setId((int) studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();
        responseObserver.onNext(response);
        System.out.println(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getAllStudents(TestService.GetAllStudentsRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {

        List<StudentEntity> studentsList = studentService.getAllStudents();
        //will return response iterator
        for(StudentEntity studentEntity: studentsList){
            TestService.GetStudentsByIdResponse response = TestService.GetStudentsByIdResponse.newBuilder()
                    .setId((int) studentEntity.getId())
                    .setName(studentEntity.getName())
                    .setLastname(studentEntity.getLastName())
                    .setSubject(studentEntity.getSubject())
                    .setCourse(studentEntity.getCourse())
                    .build();
            responseObserver.onNext(response);
            ServerInfoPublisher producer = new ServerInfoPublisher(LocalDateTime.now().toString() +
                    "  INFO: server get response: " + response);
            Thread pr = new Thread(producer);
            pr.start();
            System.out.println("get all students send");
            //System.out.println(response);
        }
        responseObserver.onCompleted();
    }

    @Override
    public void createNewStudent(TestService.AddNewStudentRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        System.out.println("request is " + request);
        System.out.println(request.getId());
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setId(request.getId());
        studentEntity.setName(request.getName());
        studentEntity.setLastName(request.getLastname());
        studentEntity.setSubject(request.getSubject());
        studentEntity.setCourse(request.getCourse());
        studentService.createNewStudent(studentEntity);
        System.out.println("add new student");
        TestService.GetStudentsByIdResponse response = TestService.GetStudentsByIdResponse.newBuilder()
                .setId((int) studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void updateStudentById(TestService.UpdateStudentByIdRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        Integer oldId = request.getOldId();
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setId(request.getNewId());
        studentEntity.setName(request.getName());
        studentEntity.setLastName(request.getLastname());
        studentEntity.setSubject(request.getSubject());
        studentEntity.setCourse(request.getCourse());
        studentService.updateStudentById(studentEntity, oldId);
        TestService.GetStudentsByIdResponse response = TestService.GetStudentsByIdResponse.newBuilder()
                .setId((int) studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void deleteStudentById(TestService.DeleteStudentByIdRequest request, StreamObserver<TestService.GetStudentsByIdResponse> responseObserver) {
        int id = request.getId();
        StudentEntity studentEntity = studentService.getStudentById(id);
        studentService.deleteStudentById(id);
        TestService.GetStudentsByIdResponse response
                = TestService.GetStudentsByIdResponse.newBuilder()
                .setId((int) studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
