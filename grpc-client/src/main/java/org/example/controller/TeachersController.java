package org.example.controller;

import com.example.grpc.TeacherServiceGrpc;
import com.example.grpc.TestService;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Delete;
import org.example.entitiy.TeacherEntity;
import org.example.serivce.TeacherService;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/teachers")
public class TeachersController {
    TeacherService teacherService = new TeacherService();

    @GetMapping("{teacherId}")
    public TeacherEntity test(@PathVariable Integer teacherId) {
        return teacherService.getTeacherById(teacherId);
    }
    @GetMapping()
    public void test1(){
        teacherService.getAllTeachers();
    }
    @PostMapping("/new")
    public TeacherEntity addNewTeacher(@RequestBody TeacherEntity teacherEntity){
        return teacherService.addNewTeacher(teacherEntity);
    }
    @DeleteMapping("{teacherId}")
    public TeacherEntity deleteTeacherById(@PathVariable Integer teacherId){
        return teacherService.deleteTeacherById(teacherId);
    }
    @PostMapping("update/{teacher_id}")
    public TeacherEntity updateTeacherById(@PathVariable Integer teacher_id,
                                           @RequestBody TeacherEntity teacherEntity){
        return teacherService.updateTeacherById(teacher_id, teacherEntity);
    }
}
