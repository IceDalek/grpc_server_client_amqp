package org.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.entitiy.StudentEntity;
import org.example.serivce.StudentService;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/students")
public class StudentsController {
    StudentService studentService = new StudentService();

    @GetMapping("{studentId}")
    public StudentEntity test(@PathVariable Integer studentId) {
        return studentService.getStudentById(studentId);
    }
    @GetMapping()
    public void test1(){
        studentService.getAllStudents();
    }
    @PostMapping("/new")
    public StudentEntity addNewstudent(@RequestBody StudentEntity studentEntity){
        return studentService.addNewStudent(studentEntity);
    }
    @DeleteMapping("{studentId}")
    public StudentEntity deletestudentById(@PathVariable Integer studentId){
        return studentService.deleteStudentById(studentId);
    }
    @PostMapping("update/{student_id}")
    public StudentEntity updateStudentById(@PathVariable Integer student_id,
                                           @RequestBody StudentEntity studentEntity){
        return studentService.updateStudent(studentEntity, student_id);
    }
}
