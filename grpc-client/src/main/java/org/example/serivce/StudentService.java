package org.example.serivce;

import com.example.grpc.StudentsServiceGrpc;
import com.example.grpc.TestService;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.example.entitiy.StudentEntity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StudentService {
    public StudentEntity getStudentById(Integer id) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.GetStudentByIdRequest request
                = TestService.GetStudentByIdRequest.newBuilder().setId(id).build();

        TestService.GetStudentsByIdResponse response = stub.getStudentById(request);
        System.out.println(response);
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setId(response.getId());
        studentEntity.setName(response.getName());
        studentEntity.setLastName(response.getLastname());
        studentEntity.setSubject(response.getSubject());
        studentEntity.setCourse(response.getCourse());
        channel.shutdownNow();
        return studentEntity;
    }

    public List<StudentEntity> getAllStudents() {
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.GetAllStudentsRequest request
                = TestService.GetAllStudentsRequest.newBuilder().setStub("").build();
        Iterator students = stub.getAllStudents(request);
        List<StudentEntity> ret = new ArrayList<>();
        while (students.hasNext()) {
            ret.add((StudentEntity) students.next());
        }
        return ret;
    }

    public StudentEntity addNewStudent(StudentEntity studentEntity) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.AddNewStudentRequest request
                = TestService.AddNewStudentRequest.newBuilder()
                .setId(studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();

        TestService.GetStudentsByIdResponse response = stub.createNewStudent(request);
        System.out.println("Add new student response: " + response);
        channel.shutdownNow();
        return studentEntity;

    }

    public StudentEntity updateStudent(StudentEntity studentEntity, Integer id) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        StudentsServiceGrpc.StudentsServiceBlockingStub stub =
                StudentsServiceGrpc.newBlockingStub(channel);
        TestService.UpdateStudentByIdRequest request
                = TestService.UpdateStudentByIdRequest.newBuilder().
                setOldId(id).
                setNewId(studentEntity.getId())
                .setName(studentEntity.getName())
                .setLastname(studentEntity.getLastName())
                .setSubject(studentEntity.getSubject())
                .setCourse(studentEntity.getCourse())
                .build();

        TestService.GetStudentsByIdResponse response = stub.updateStudentById(request);
        System.out.println("server send update response + " + response);
        StudentEntity ret = new StudentEntity();
        ret.setId(response.getId());
        ret.setName(response.getName());
        ret.setLastName(response.getLastname());
        ret.setSubject(response.getSubject());
        ret.setCourse(request.getCourse());
        channel.shutdownNow();
        return studentEntity;
    }
    public StudentEntity deleteStudentById(Integer id){
        return null;
    }

}
