package org.example.serivce;

import com.example.grpc.TeacherServiceGrpc;
import com.example.grpc.TestService;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.example.entitiy.TeacherEntity;
import java.util.List;
import java.util.Iterator;

public class TeacherService {
    public TeacherEntity getTeacherById(Integer id){
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        TeacherServiceGrpc.TeacherServiceBlockingStub stub =
                TeacherServiceGrpc.newBlockingStub(channel);
        TestService.GetTeacherByIdRequest request
                = TestService.GetTeacherByIdRequest.newBuilder().setId(id).build();

        TestService.GetTeacherByIdResponse response = stub.getTeacherById(request);
        System.out.println(response);
        TeacherEntity teacherEntity = new TeacherEntity();
        teacherEntity.setId(response.getId());
        teacherEntity.setName(response.getName());
        teacherEntity.setLastName(response.getLastname());
        teacherEntity.setSubject(response.getSubject());
        channel.shutdownNow();
        return teacherEntity;
    }
    public List<TeacherEntity> getAllTeachers(){
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        TeacherServiceGrpc.TeacherServiceBlockingStub stub =
                TeacherServiceGrpc.newBlockingStub(channel);
        TestService.GetAllTeachersRequest request
               = TestService.GetAllTeachersRequest.newBuilder().setStub("").build();
        Iterator teachers = stub.getAllTeachers(request);
        while(teachers.hasNext()){
            System.out.println(teachers.next());
        }
        return null;
    }

    public TeacherEntity addNewTeacher(TeacherEntity teacherEntity){
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        TeacherServiceGrpc.TeacherServiceBlockingStub stub =
                TeacherServiceGrpc.newBlockingStub(channel);
        TestService.AddNewTeacherRequest request
                = TestService.AddNewTeacherRequest.newBuilder()
                .setId(teacherEntity.getId())
                .setName(teacherEntity.getName())
                .setLastname(teacherEntity.getLastName())
                .setSubject(teacherEntity.getSubject())
                .build();

        TestService.GetTeacherByIdResponse response = stub.createNewTeacher(request);
        System.out.println("Add new teacher response: " + response);
        channel.shutdownNow();
        return teacherEntity;
    }
    public TeacherEntity deleteTeacherById(Integer id){

        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        TeacherServiceGrpc.TeacherServiceBlockingStub stub =
                TeacherServiceGrpc.newBlockingStub(channel);
        TestService.DeleteTeacherByIdRequest request
                = TestService.DeleteTeacherByIdRequest.newBuilder().setId(id).build();

        TestService.GetTeacherByIdResponse response = stub.deleteTeacherById(request);
        System.out.println("resver send delete response + " + response);
        TeacherEntity teacherEntity = new TeacherEntity();
        teacherEntity.setId(response.getId());
        teacherEntity.setName(response.getName());
        teacherEntity.setLastName(response.getLastname());
        teacherEntity.setSubject(response.getSubject());
        channel.shutdownNow();
        return teacherEntity;
    }
    public TeacherEntity updateTeacherById(Integer id, TeacherEntity teacherEntity){

        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8020")
                .usePlaintext().build();
        TeacherServiceGrpc.TeacherServiceBlockingStub stub =
                TeacherServiceGrpc.newBlockingStub(channel);
        TestService.UpdateTeacherByIdRequest request
                = TestService.UpdateTeacherByIdRequest.newBuilder().
                setOldId(id).
                setNewId(teacherEntity.getId())
                .setName(teacherEntity.getName())
                .setLastname(teacherEntity.getLastName())
                .setSubject(teacherEntity.getSubject())
                .build();

        TestService.GetTeacherByIdResponse response = stub.updateTeacherById(request);
        System.out.println("server send update response + " + response);
        TeacherEntity ret = new TeacherEntity();
        ret.setId(response.getId());
        ret.setName(response.getName());
        ret.setLastName(response.getLastname());
        ret.setSubject(response.getSubject());
        channel.shutdownNow();
        return teacherEntity;
    }
}
